
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 09/29/2012 16:53:28
-- Generated from EDMX file: Z:\Projects\3AM\DFID - Data\src\DFID.Data.Model\Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [DFID];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Country_Points]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Points] DROP CONSTRAINT [FK_Country_Points];
GO
IF OBJECT_ID(N'[dbo].[FK_Region_Points]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Points] DROP CONSTRAINT [FK_Region_Points];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Regions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Regions];
GO
IF OBJECT_ID(N'[dbo].[Countries]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Countries];
GO
IF OBJECT_ID(N'[dbo].[Points]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Points];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Regions'
CREATE TABLE [dbo].[Regions] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Latitude] float  NOT NULL,
    [Longitude] float  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Countries'
CREATE TABLE [dbo].[Countries] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Latitude] float  NOT NULL,
    [Longitude] float  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Points'
CREATE TABLE [dbo].[Points] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Value] float  NOT NULL,
    [Type] tinyint  NOT NULL,
    [Country_Id] bigint  NOT NULL,
    [Region_Id] bigint  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Regions'
ALTER TABLE [dbo].[Regions]
ADD CONSTRAINT [PK_Regions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Countries'
ALTER TABLE [dbo].[Countries]
ADD CONSTRAINT [PK_Countries]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Points'
ALTER TABLE [dbo].[Points]
ADD CONSTRAINT [PK_Points]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Country_Id] in table 'Points'
ALTER TABLE [dbo].[Points]
ADD CONSTRAINT [FK_Country_Points]
    FOREIGN KEY ([Country_Id])
    REFERENCES [dbo].[Countries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Country_Points'
CREATE INDEX [IX_FK_Country_Points]
ON [dbo].[Points]
    ([Country_Id]);
GO

-- Creating foreign key on [Region_Id] in table 'Points'
ALTER TABLE [dbo].[Points]
ADD CONSTRAINT [FK_Region_Points]
    FOREIGN KEY ([Region_Id])
    REFERENCES [dbo].[Regions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Region_Points'
CREATE INDEX [IX_FK_Region_Points]
ON [dbo].[Points]
    ([Region_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------