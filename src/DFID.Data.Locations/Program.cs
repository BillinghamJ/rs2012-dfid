﻿using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using DFID.Data.Data.Impl;
using DFID.Data.Model;
using Newtonsoft.Json;

namespace DFID.Data.Locations
{
  class Program
  {
    static void Main(string[] args)
    {
      var db = new ModelContainer();
      var countryRepo = new CountryRepository(db, db.Countries);

      var countries = countryRepo.GetAll().Where(e => e.Latitude == 0 || e.Longitude == 0).ToList();

      foreach (var country in countries)
      {
        var request =
          WebRequest.CreateHttp(
            "https://maps.googleapis.com/maps/api/place/textsearch/json?key=AIzaSyD32PIjyFB-ovDZG5JvaKQYqUxCCuGru6k&sensor=false&query=" +
            HttpUtility.UrlEncode(country.Name));
        var response = request.GetResponse();
        var responseStream = response.GetResponseStream();

        if (responseStream == null)
          continue;

        var json = new StreamReader(responseStream).ReadToEnd();
        var result = JsonConvert.DeserializeAnonymousType(json, new { results = new[] { new { formatted_address = string.Empty, types = new string[] { }, geometry = new { location = new { lat = double.NaN, lng = double.NaN } } } } });

        if (result.results.Count() != 1)
          continue;

        var place = result.results.First();

        if (!place.types.Contains("country"))
          continue;

        countryRepo.Change(country, delegate(ref Country e)
        {
          e.Name = place.formatted_address;
          e.Latitude = place.geometry.location.lat;
          e.Longitude = place.geometry.location.lng;
        });
      }

      db.SaveChanges();
    }
  }
}
