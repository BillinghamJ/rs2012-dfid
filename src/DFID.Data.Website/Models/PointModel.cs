using AutoMapper;
using DFID.Data.Model;

namespace DFID.Data.Website.Models
{
  public class PointModel
  {
    public static PointModel Map(Point point, double lowValue, double highValue)
    {
      Mapper.CreateMap<Point, PointModel>()
        .ForMember(e => e.Name, m => m.MapFrom(e => e.Region.Name))
        .ForMember(e => e.Latitude, m => m.MapFrom(e => e.Region.Latitude))
        .ForMember(e => e.Longitude, m => m.MapFrom(e => e.Region.Longitude));

      var result = Mapper.Map<PointModel>(point);
      result.Normalized = Normalize(result.Value, lowValue, highValue);
      return result;
    }

    private static double Normalize(double value, double lowValue, double highValue)
    {
      var range = highValue - lowValue;
      return (9 * (value - lowValue) / range) + 1;
    }

    public string Name { get; set; }
    public PointType Type { get; set; }
    public double Latitude { get; set; }
    public double Longitude { get; set; }
    public double Value { get; set; }
    public double Normalized { get; set; }
  }
}
