﻿using System.Linq;
using AutoMapper;
using DFID.Data.Model;

namespace DFID.Data.Website.Models
{
  public class CountryModel
  {
    public static CountryModel Map(Country country)
    {
      var min = country.Points.Min(e => e.Value);
      var max = country.Points.Max(e => e.Value);

      Mapper.CreateMap<Country, CountryModel>()
        .ForMember(e => e.Points, m => m.MapFrom(e => new PointModel[] { }));

      var result = Mapper.Map<CountryModel>(country);
      result.Points = country.Points.Select(p => PointModel.Map(p, min, max)).ToArray();
      return result;
    }

    public string Name { get; set; }
    public double Latitude { get; set; }
    public double Longitude { get; set; }
    public PointModel[] Points { get; set; }
  }
}
