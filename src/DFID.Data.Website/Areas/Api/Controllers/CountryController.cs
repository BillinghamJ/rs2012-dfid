﻿using System.Linq;
using System.Web.Mvc;
using DFID.Data.Data;
using DFID.Data.Website.Models;
using Microsoft.Practices.ServiceLocation;

namespace DFID.Data.Website.Areas.Api.Controllers
{
  public class CountryController : Controller
  {
    private ICountryRepository _countryRepository;
    public ICountryRepository CountryRepository
    {
      get { return _countryRepository ?? (_countryRepository = ServiceLocator.Current.GetInstance<ICountryRepository>()); }
    }

    public JsonResult Get(long id)
    {
      var country = CountryRepository.Get(id);
      return Json(CountryModel.Map(country), JsonRequestBehavior.AllowGet);
    }

    public JsonResult GetAll()
    {
      return Json(CountryRepository.GetAll().ToDictionary(e => e.Id.ToString(), e => e.Name), JsonRequestBehavior.AllowGet);
    }
  }
}
