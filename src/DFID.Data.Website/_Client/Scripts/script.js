$(document).ready(function () {
  $('#country').change(function() {
    window.location.href = '/' + $(this).val();
  });

  setTimeCounterOn();

  if ($('#contact_map').length) {
    startGmap();
    $('body').addClass('map');
  }

  if (typeof twitter_username !== "undefined" && twitter_username != '') {
    $("footer .twitter-feed").tweet({
      join_text: "",
      count: 1,
      loading_text: "loading tweets...",
      username: twitter_username,
      template: "{text}{join}{time}"
    });
  }
  else {
    $('.tweet_list').hide();
  }

  $('form').submit(function () {
    var form_data = $(this).serialize();

    if (validateEmail($('input[name=email]').attr('value'))) {
      if (typeof ajax_form !== "undefined" && ajax_form === true) {
        $.post($(this).attr('action'), form_data, function (data) {
          $('form').fadeOut('slow', function () { $(this).after('<p class="form-msg">' + data + '</p>'); });
          $('.spam').html('&nbsp;');
        });

        return false;
      }
    }
    else {
      $('p.spam').text('Please enter a valid e-mail').effect("pulsate", { times: 3 }, 1000);
      return false;
    }
  });

  if (typeof slideshow_images !== "undefined" && slideshow_images != '') {
    $('body').addClass('slide');

    if (typeof slideshow_transition === "undefined") {
      slideshow_transition = 'none';
    }

    if (typeof slideshow_transition_speed === "undefined") {
      slideshow_transition_speed = 1000;
    }

    if (typeof slideshow_slide_interval === "undefined") {
      slideshow_slide_interval = 5000;
    }

    $.supersized({

      slideshow: 1, // Slideshow on/off
      autoplay: 1, // Slideshow starts playing automatically
      start_slide: 1, // Start slide (0 is random)
      stop_loop: 0, // Pauses slideshow on last slide
      random: 0, // Randomize slide order (Ignores start slide)
      slide_interval: slideshow_slide_interval, // Length between transitions
      transition: slideshow_transition, // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
      transition_speed: slideshow_transition_speed, // Speed of transition
      new_window: 1, // Image links open in new window/tab
      pause_hover: 0, // Pause slideshow on hover
      keyboard_nav: 1, // Keyboard navigation on/off
      performance: 1, // 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
      image_protect: 1, // Disables image dragging and right click with Javascript

      // Size & Position
      min_width: 0, // Min width allowed (in pixels)
      min_height: 0, // Min height allowed (in pixels)
      vertical_center: 1, // Vertically center background
      horizontal_center: 1, // Horizontally center background
      fit_always: 0, // Image will never exceed browser width or height (Ignores min. dimensions)
      fit_portrait: 1, // Portrait images will not exceed browser height
      fit_landscape: 0, // Landscape images will not exceed browser width

      // Components
      slide_links: 'blank', // Individual links for each slide (Options: false, 'num', 'name', 'blank')
      thumb_links: 1, // Individual thumb links for each slide
      thumbnail_navigation: 0, // Thumbnail navigation
      slides: slideshow_images,

      // Theme Options
      progress_bar: 1, // Timer for each slide
      mouse_scrub: 0
    });
  }
  else {
    if (typeof timeTo !== "undefined" && timeTo != '') {
      $('.timer-background').show();
    }

    $('body').removeClass('slide');
    $('#progress-back, #supersized-loader, #supersized').hide();
  }

  footer_height();
  map_height();

  $(window).resize(function () {
    footer_height();
    map_height();
  });
});

Number.prototype.toCurrency = function ($O) { // extending Number prototype

  String.prototype.separate_thousands = function () { // Thousands separation
    $val = this;
    var rx = new RegExp('(-?[0-9]+)([0-9]{3})');
    while (rx.test($val)) { $val = $val.replace(rx, '$1' + $O.thousands_separator + '$2'); }
    return $val;
  }

  Number.prototype.toFixed = function () { // Rounding
    var m = Math.pow(10, $O.use_fractions.fractions);
    return Math.round(this * m, 0) / m;
  }

  String.prototype.times = function (by) { // String multiplication
    by = (by >> 0);
    var t = (by > 1 ? this.times(by / 2) : '');
    return t + (by % 2 ? t + this : t);
  }

  var $A = this;

  /* I like to keep all options, as the name would sugesst, **optional** :) so, let me make tham as such */
  $O ? null : $O = new Object;
  /* If no thousands_separator is present default to "," */
  $O.thousands_separator ? null : $O.thousands_separator = ",";
  /* If no currency_symbol is present default to "$" */
  $O.currency_symbol ? null : $O.currency_symbol = "$";

  // Fractions use is separated, just in case you don't want them
  if ($O.use_fractions) {
    $O.use_fractions.fractions ? null : $O.use_fractions.fractions = 2;
    $O.use_fractions.fraction_separator ? null : $O.use_fractions.fraction_separator = ".";
  } else {
    $O.use_fractions = new Object;
    $O.use_fractions.fractions = 0;
    $O.use_fractions.fraction_separator = " ";
  }
  // We round this number
  $A.round = $A.toFixed();

  // We convert rounded Number to String and split it to integrer and fractions
  $A.arr = ($A.round + "").split(".");
  // First part is an integrer
  $A._int = $A.arr[0].separate_thousands();
  // Second part, if exists, are rounded decimals
  $A.arr[1] == undefined ? $A._dec = $O.use_fractions.fraction_separator + "0".times($O.use_fractions.fractions) : $A._dec = $O.use_fractions.fraction_separator + $A.arr[1];

  /* If no symbol_position is present, default to "front" */
  $O.symbol_position ? null : $O.symbol_position = "front";
  $O.symbol_position == "front" ? $A.ret = $O.currency_symbol + $A._int + $A._dec : $A.ret = $A._int + $A._dec + " " + $O.currency_symbol;
  return $A.ret;
}

function footer_height() {
  h_body = $(window).height();
  h_main = $(".container-comming-soon").outerHeight();
  h_footer = (h_body - h_main);
  $(".slide footer").css({ "min-height": h_footer });
};

function map_height() {
  var window_h = $(window).height();
  //var html_h = $('html').height();
  var footer_h = $('footer').height();
  var html_h = $(document).height();

  /*alert('window: ' + window_h);
	alert('html: ' + html_h);
	alert('document: ' + document_h);*/

  if ($('body').hasClass('footer-transparent')) {
    var content_h = $('.content').outerHeight(true);
    var elements_h = $('.tweet_list').outerHeight(true);

    if (content_h > (window_h - elements_h) - 240) {
      $('.bottom-elements, .rights-reserved').removeClass('bottom');
    }
    else {
      $('.bottom-elements, .rights-reserved').addClass('bottom');
    }

    if (window_h > html_h) map_h = window_h;
    else map_h = html_h;

    $('.contact_map').css({ 'height': map_h });
  }
  else {
    $('.contact_map').css({ 'height': (html_h - (footer_h - 30)) });
  }

  if ($('body').hasClass('footer-transparent') && !$('.bottom-elements').hasClass('bottom')) {
    social_h = $('.social').outerHeight();
    $('.social').css({ 'margin-top': -(social_h + 50) });
  }
}

function setTimeCounterOn() {
  var days = 24 * 60 * 60,
    hours = 60 * 60,
    minutes = 60;
  if (typeof timeTo !== "undefined" && timeTo != '')
    var e = Math.floor((timeTo - (new Date())) / 1000); if (e < 0) { e = 0 } var c = Math.floor(e / days); updateElement("days", c); e -= c * days; var b = Math.floor(e / hours); updateElement("hours", b); e -= b * hours; var a = Math.floor(e / minutes); updateElement("minutes", a); e -= a * minutes; var d = e; updateElement("seconds", d); setTimeout(setTimeCounterOn, 1000);
}

function updateElement(a, b) {
  if (a == "days") { $(".days").text(b) } else { if (a == "hours") { $(".hours").text(b) } else { if (a == "minutes") { $(".minutes").text(b) } else { if (a == "seconds") { $(".seconds").prepend('<i style="display:none">' + b + "</i>"); $(".seconds i").last().hide(200); $(".seconds i").first().show(200); $(".seconds i").last().remove() } } } };
}

if (typeof timeTo !== "undefined" && timeTo != '') {
  setTimeCounterOn();
}
else {
  setTimeCounterOff();
}

function setTimeCounterOff() {
  $('.timer-background').hide();
  return false;
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function startGmap() {
  var myOptions = {
    zoom: 3,
    center: new google.maps.LatLng(google_maps_latitude, google_maps_longitude),
    navigationControlOptions: {
      style: google.maps.NavigationControlStyle.NORMAL,
      position: google.maps.ControlPosition.RIGHT_TOP
    },
    streetViewControl: false,
    scrollwheel: false,
    zoomControl: true,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.DEFAULT,
      position: google.maps.ControlPosition.RIGHT_TOP
    },
    mapTypeControl: false,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
      position: google.maps.ControlPosition.TOP_RIGHT,
      mapTypeIds: ["ptMap"]
    }
  };

  map = new google.maps.Map(document.getElementById('contact_map'), myOptions);

  var styleCP = [
		{ featureType: "administrative", elementType: "all", stylers: [{ visibility: "off" }] },
		{ featureType: 'landscape', elementType: 'all', stylers: [{ color: google_maps_landscape_color }, { visibility: 'on' }] },
		{ featureType: "poi", elementType: "all", stylers: [{ visibility: "off" }] },
		{ featureType: "road", elementType: "all", stylers: [{ visibility: "on" }, { lightness: -30 }] },
		{ featureType: "transit", elementType: "all", stylers: [{ visibility: "off" }] },
		{ featureType: "water", elementType: "all", stylers: [{ color: google_maps_water_color }] }
  ];

  var styledMapOptions = { name: "Map" };
  var ptMapType = new google.maps.StyledMapType(styleCP, styledMapOptions);
  map.mapTypes.set("ptMap", ptMapType);
  map.setMapTypeId("ptMap");

  var circle = {
    path: google.maps.SymbolPath.CIRCLE,
    fillOpacity: 1,
    fillColor: google_maps_circle_color,
    strokeOpacity: 1.0,
    strokeColor: google_maps_circle_color,
    strokeWeight: 1.0,
    scale: 10
  };

  var point = new google.maps.LatLng(google_maps_latitude, google_maps_longitude);

  var marker = new google.maps.Marker({
    position: point,
    map: map,
    zIndex: 99999,
    optimized: false,
    icon: circle,
    title: country.name
  });

  var infoWindow = new google.maps.InfoWindow();
  
  for (var i = 0; i < country.points.length; i++) {
    var region = country.points[i];
    var regionLoc = new google.maps.LatLng(region.latitude, region.longitude);

    var polyline = new google.maps.Polyline({
      path: [point, regionLoc],
      strokeColor: "#008bda",
      strokeOpacity: 0.75,
      strokeWeight: region.normalized,
      geodesic: true,
      map: map
    });

    var marker_ = new google.maps.Marker({
      position: regionLoc,
      map: map,
      title: region.name,
      zIndex: 99999,
      optimized: false,
      icon: circle,
      info: region
    });
    
    google.maps.event.addListener(marker_, 'click', function () {
      infoWindow.setContent('<div><h3>' + this.info.name + '</h3><h4>' + this.info.value.toCurrency({
        "thousands_separator":",",
        "currency_symbol":"$",
        "symbol_position":"front",
        "use_fractions" : { "fractions":2, "fraction_separator":"." }
      }) + '</h4></div>');
      infoWindow.open(map, this);
    });
  }
}
