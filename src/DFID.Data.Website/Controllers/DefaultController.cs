﻿using System.Linq;
using System.Web.Mvc;
using DFID.Data.Data;
using DFID.Data.Website.Models;
using Microsoft.Practices.ServiceLocation;

namespace DFID.Data.Website.Controllers
{
  public class DefaultController : Controller
  {
    private ICountryRepository _countryRepository;
    public ICountryRepository CountryRepository
    {
      get { return _countryRepository ?? (_countryRepository = ServiceLocator.Current.GetInstance<ICountryRepository>()); }
    }

    public ActionResult Index(long? id)
    {
      var country = id.HasValue ? CountryRepository.Get(id.Value) : CountryRepository.GetAll().Single(e => e.Name == "United Kingdom");

      ViewBag.Country = country.Id;
      ViewBag.Countries = CountryRepository.GetAll().ToDictionary(e => e.Id, e => e.Name);
      return View(CountryModel.Map(country));
    }
  }
}
