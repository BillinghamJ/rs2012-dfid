﻿using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Routing;
using CommonServiceLocator.NinjectAdapter;
using DFID.Data.Data;
using DFID.Data.Data.Impl;
using DFID.Data.Model;
using Microsoft.Practices.ServiceLocation;
using Ninject;
using Tam.Lib.Model;

namespace DFID.Data.Website
{
  public class MvcApplication : System.Web.HttpApplication
  {
    public static void RegisterRoutes(RouteCollection routes)
    {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

      routes.MapRoute(
          "Default", // Route name
          "{id}", // URL with parameters
          new { controller = "Default", action = "Index", id = UrlParameter.Optional } // Parameter defaults
      );
    }

    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();
      RegisterRoutes(RouteTable.Routes);

      IKernel ninject = new StandardKernel();
      ninject.Bind<IDbContext>().To<ModelContainer>();
      ninject.Bind<IDbSet<Country>>().ToMethod(e => ((ModelContainer)ServiceLocator.Current.GetInstance<IDbContext>()).Countries);
      ninject.Bind<ICountryRepository>().To<CountryRepository>();

      var locator = new NinjectServiceLocator(ninject);
      ServiceLocator.SetLocatorProvider(() => locator);
    }
  }
}
