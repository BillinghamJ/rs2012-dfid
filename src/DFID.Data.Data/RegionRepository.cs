//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DFID.Data.Data.Impl
{
  using System.Data.Entity;
  using Tam.Lib.Data;
  using Tam.Lib.Model;
  using DFID.Data.Model;
  
  public partial class RegionRepository : ARepository<Region>, IRegionRepository
  {
    public RegionRepository(IDbContext database, IDbSet<Region> data)
      : base(database, data)
    {
    }
  }
}
