﻿using System;
using DFID.Data.Data.Impl;
using DFID.Data.Model;
using FileHelpers;

namespace DFID.Data.Import
{
  class Program
  {
    static void Main(string[] args)
    {
      var engine = new FileHelperEngine<Country>();
      var db = new ModelContainer();
      var regionRepo = new RegionRepository(db, db.Regions);
      var countryRepo = new CountryRepository(db, db.Countries);
      var pointRepo = new PointRepository(db, db.Points);

      var latinAmerica = regionRepo.Get(2);
      var middleEast = regionRepo.Get(3);
      var southAsia = regionRepo.Get(4);
      var subsaharanAfrica = regionRepo.Get(6);

      var data = engine.ReadFile("data.csv");

      foreach (var country in data)
      {
        double exports;
        double countryLatinAmerica;
        double countryMiddleEast;
        double countrySouthAsia;
        double countrySubSaharanAfrica;

        if (!(
          double.TryParse(country.Merchandiseexports, out exports)
          && double.TryParse(country.MerchandiseexportstodevelopingeconomiesinLatinAmericaTheCaribbean, out countryLatinAmerica)
          && double.TryParse(country.MerchandiseexportstodevelopingeconomiesinMiddleEastNorthAfrica, out countryMiddleEast)
          && double.TryParse(country.MerchandiseexportstodevelopingeconomiesinSouthAsia, out countrySouthAsia)
          && double.TryParse(country.MerchandiseexportstodevelopingeconomiesinSubSaharanAfrica, out countrySubSaharanAfrica)))
          continue;

        if (
          Math.Abs(exports) < double.Epsilon
          || Math.Abs(countryLatinAmerica) < double.Epsilon
          || Math.Abs(countryMiddleEast) < double.Epsilon
          || Math.Abs(countrySouthAsia) < double.Epsilon
          || Math.Abs(countrySubSaharanAfrica) < double.Epsilon)
          continue;

        exports /= 100;
        countryLatinAmerica *= exports;
        countryMiddleEast *= exports;
        countrySouthAsia *= exports;
        countrySubSaharanAfrica *= exports;

        var dbCountry = countryRepo.Create(new Model.Country
        {
          Name = country.Name,
          Latitude = 0,
          Longitude = 0
        });

        pointRepo.Create(new Point
        {
          Region = latinAmerica,
          Country = dbCountry,
          Type = PointType.Merchandise,
          Value = countryLatinAmerica
        });

        pointRepo.Create(new Point
        {
          Region = middleEast,
          Country = dbCountry,
          Type = PointType.Merchandise,
          Value = countryMiddleEast
        });

        pointRepo.Create(new Point
        {
          Region = southAsia,
          Country = dbCountry,
          Type = PointType.Merchandise,
          Value = countrySouthAsia
        });

        pointRepo.Create(new Point
        {
          Region = subsaharanAfrica,
          Country = dbCountry,
          Type = PointType.Merchandise,
          Value = countrySubSaharanAfrica
        });
      }

      db.SaveChanges();
    }
  }

  [DelimitedRecord(",")]
  [IgnoreFirst(2)]
  public class Country
  {
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Region;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Name;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Accesstoelectricity;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Adolescentfertilityrate;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Agedependencyratio;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Agriculturalland;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Agriculturalrawmaterialsexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Agriculturalrawmaterialsimports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Agriculturevalueadded;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Arableland;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Averagetimetoclearexportsthroughcustoms;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string BurdenofcustomsprocedureWEF;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Commercialbankbranches;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Commercialserviceexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Commercialserviceimports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Consumerpriceindex;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Containerporttraffic;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Costofbusinessstartupprocedures;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Costtoexport;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Costtoimport;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAbuildinghumanresourcesrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAbusinessregulatoryenvironmentrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAdebtpolicyrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAeconomicmanagementclusteraverage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAefficiencyofrevenuemobilizationrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAequityofpublicresourceuserating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAfinancialsectorrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAfiscalpolicyrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAgenderequalityrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAmacroeconomicmanagementrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIApoliciesforsocialinclusionequityclusteraverage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIApolicyandinstitutionsforenvironmentalsustainabilityrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIApropertyrightsandrulebasedgovernancerating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIApublicsectormanagementandinstitutionsclusteraverage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAqualityofbudgetaryandfinancialmanagementrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAqualityofpublicadministrationrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAsocialprotectionrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAstructuralpoliciesclusteraverage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAtraderating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CPIAtransparencyaccountabilityandcorruptioninthepublicsectorrating;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Currentaccountbalance;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string CustomsandotherimportdutiesPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Customsandotherimportduties;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Documentstoexport;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Documentstoimport;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Employmentinagriculture;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Employmentinindustry;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Employmentinservices;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ExpensePercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Expense;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ExportsofgoodsandservicesPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Exportsofgoodsandservices;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Fertilityratetotal;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Firmsexpectedtogivegiftsinmeetingswithtaxofficials;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Firmswithfemaleparticipationinownership;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string FixedbroadbandInternetsubscriber;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string FixedbroadbandInternetsubscribers;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Foodexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Foodimports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Foreigndirectinvestmentnet;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ForeigndirectinvestmentnetinflowsPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Foreigndirectinvestmentnetinflows;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Foreigndirectinvestmentnetoutflows;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Fuelexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Fuelimports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string GDP;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string GDPgrowth;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string GDPpercapita;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string GDPpercapitagrowth;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string GINIinde;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Goodsexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Goodsimports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string HealthexpenditurepublicPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Healthexpenditurepublic;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string HightechnologyexportsPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Hightechnologyexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string HouseholdfinalconsumptionexpenditureGDP;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Householdfinalconsumptionexpenditure;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ICTgoodsexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ICTgoodsimports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ICTserviceexportsPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ICTserviceexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ImportsofgoodsandservicesPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ImportsofgoodsandservicesPercentageGrowth;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Importsofgoodsandservices;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Incomeshareheldbyhighest10Percent;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string IndustryvalueaddedPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string IndustryvalueaddedPercentageGrowth;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Industryvalueadded;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Inflationconsumerprices;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Informalpaymentstopublicofficials;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string InternationaltourismexpendituresPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Internationaltourismexpenditures;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string InternationaltourismreceiptsPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Internationaltourismreceipts;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Internetusers;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Laborforcewithprimaryeducation;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Laborforcewithsecondaryeducation;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Laborforcewithtertiaryeducation;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Leadtimetoexportmediancase;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Leadtimetoimportmediancase;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Lifeexpectancyatbirthtotal;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Linershippingconnectivityindex;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Literacyrateadulttotal;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string LogisticsperformanceindexAbilitytotrackandtraceconsignments;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string LogisticsperformanceindexCompetenceandqualityoflogisticsservices;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string LogisticsperformanceindexEaseofarrangingcompetitivelypricedshipments;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string LogisticsperformanceindexEfficiencyofcustomsclearanceprocess;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string LogisticsperformanceindexFrequencywithwhichshipmentsreachconsigneewithinscheduledorexpectedtime;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string LogisticsperformanceindexOverall;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string LogisticsperformanceindexQualityoftradeandtransportrelatedinfrastructure;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Managementtimedealingwithofficials;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Manufacturesexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Manufacturesimports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ManufacturingvalueaddedPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ManufacturingvalueaddedPercentageGrowth;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Manufacturingvalueadded;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MarketAccessOverallTradeRestrictivenessIndex;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MarketAccessOverallTradeRestrictivenessIndexagriculture;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MarketAccessOverallTradeRestrictivenessIndexmanufacturing;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MarketAccessTariffTradeRestrictivenessIndexMATTRI;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MarketAccessOverallTradeRestrictivenessIndexagricultureMATTRI;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MarketAccessOverallTradeRestrictivenessIndexmanufacturingMATTRI;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Merchandiseexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MerchandiseexportstodevelopingeconomiesinLatinAmericaTheCaribbean;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MerchandiseexportstodevelopingeconomiesinMiddleEastNorthAfrica;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MerchandiseexportstodevelopingeconomiesinSouthAsia;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MerchandiseexportstodevelopingeconomiesinSubSaharanAfrica;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Merchandiseexportstodevelopingeconomiesoutsideregion;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Merchandiseexportstodevelopingeconomieswithinregion;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MerchandiseexportstoeconomiesintheArabWorld;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Merchandiseexportstohighincomeeconomies;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Merchandiseimports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MerchandiseimportsfromdevelopingeconomiesinLatinAmericaTheCaribbean;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MerchandiseimportsfromdevelopingeconomiesinMiddleEastNorthAfrica;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MerchandiseimportsfromdevelopingeconomiesinSouthAsia;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MerchandiseimportsfromdevelopingeconomiesinSubSaharanAfrica;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Merchandiseimportsfromdevelopingeconomiesoutsideregion;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Merchandiseimportsfromdevelopingeconomieswithinregion;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MerchandiseimportsfromeconomiesintheArabWorld;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Merchandiseimportsfromhighincomeeconomies;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Merchandisetrade;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string MilitaryexpenditurePercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Militaryexpenditure;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Mineralrents;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Mobilecellularsubscriptions;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Motorvehicles;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string NetODAreceivedPercentageGovt;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string NetODAreceivedPercentageGNI;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string NetODAreceivedPercentageImports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Netofficialdevelopmentassistancereceived;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Newbusinessesregistered;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Officialexchangerate;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string OverallTradeRestrictivenessIndexal;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string OverallTradeRestrictivenessIndexagricultur;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string OverallTradeRestrictivenessIndexmanufacturing;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Populationages14;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Populationages64;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Populationagesabove;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Populationdensity;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Populationgrowth;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Populationtota;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Povertyheadcountratioat2aday;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Procedurestoenforceacontract;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Procedurestoregisterproperty;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Proportionofseatsheldbywomeninnationalparliaments;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string PublicandpubliclyguaranteeddebtservicePercentageExports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string PublicandpubliclyguaranteeddebtservicePercentageGNI;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Publicspendingoneducationtotal;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Pumppricefordieselfuel;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Pumppriceforgasoline;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string QualityofportinfrastructureWEF;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Raillines;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Ratioofgirlstoboysinprimaryandsecondaryeducation;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Revenueexcludinggrants;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Roadspaved;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Ruralpopulatio;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Ruralpopulation;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Ruralpopulationgrowth;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Schoolenrollmentprimary;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Schoolenrollmentsecondary;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Schoolenrollmenttertiary;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Serviceexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Serviceimports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ServicesetcvalueaddedPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string ServicesetcvalueaddedPercentageGrowth;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Servicesetcvalueadded;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Startupprocedurestoregisterabusiness;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string TariffTradeRestrictivenessInde;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string TariffTradeRestrictivenessIndexagricultur;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string TariffTradeRestrictivenessIndexmanufactured;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string TaxrevenuePercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Taxrevenue;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string TaxesonexportsPercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Taxesonexports;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string TaxesoninternationaltradePercentage;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Taxesoninternationaltrade;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Teenagemothers;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Telephonelines;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Timerequiredtoobtainanoperatinglicense;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Timerequiredtoregisterproperty;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Timerequiredtostartabusiness;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Timetoexport;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Timetoimport;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Timetoprepareandpaytaxes;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Timetoresolveinsolvency;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Totaltaxrate;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Trade;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Tradeinservices;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Unemploymenttotal;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Urbanpopulatio;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Urbanpopulation;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Urbanpopulationgrowth;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Wholesalepriceindex;
    [FieldQuoted('"', QuoteMode.OptionalForBoth)] public string Workersremittancesreceipts;
  }
}
